# wlan-mon-toggle

Simple sh script to toggle monitor mode on wlan interfaces using iw

Simply add to your $PATH, drop it in /bin or run it directly.
